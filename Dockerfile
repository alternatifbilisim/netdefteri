FROM debian:sid

RUN apt-get update
RUN apt-get install -y curl gcc gnupg
RUN curl -sL https://deb.nodesource.com/setup | bash -
RUN apt-get install -y ruby ruby-dev make nodejs build-essential
RUN apt-get install -y zlib1g-dev

WORKDIR /data

RUN gem install jekyll http json rdiscount bundler --no-ri --no-rdoc

ADD Gemfile* /data/

RUN bundle install

ADD . /data/
ADD _config.docker.yml /data/_config.yml

CMD ["bundle", "exec", "jekyll", "serve", "--host", "0.0.0.0", "--port", "80" ]
