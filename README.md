# Netdefteri

[Jekyll-Uno](https://github.com/joshgerdes/jekyll-uno/archive/master.zip) based blog.

### Install and Test

1. Download or clone repo `git clone https://github.com/AlternatifBilisim/netdefteri.git`
2. Enter the folder: `cd netdefteri/`
3. If you don't have bundler installed: `gem install bundler`
3. Install Ruby gems: `bundle install`
4. Start Jekyll server: `bundle exec jekyll serve --watch`

Access via: [http://localhost:4000/netdefteri/](http://localhost:4000/netdefteri/)

### Copyright and license

It is under [the GPL v3 license](/LICENSE).
